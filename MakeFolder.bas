Attribute VB_Name = "MakeFolder"

' ***** Programa cria pastas com nome de dados selecionados na planilha *****

    ' S� funciona com formatos de dados preenchidos como coluna ou matriz
    ' As pastas s�o criadas no mesmo diret�rio do arquivo excel macro
    
Sub MakeFolders()
Attribute MakeFolders.VB_ProcData.VB_Invoke_Func = "b\n14"

    ' Declara��o de vari�veis
    Dim Rng As Range
    Dim maxRows, maxCols, r, c As Integer
    Set Rng = Selection
    maxRows = Rng.Rows.Count
    maxCols = Rng.Columns.Count


    ' Loop for varre coluna a coluna
    For c = 1 To maxCols
        
        ' Loop While varre linha a linha
        r = 1
        Do While r <= maxRows
        
            ' Fun��o Dir ( Diret�rio ativo, atributo opcional [vbDirectory = 16 = Especifica diret�rios ou pastas])
            ' Este If verifica se j� n�o existe uma pasta com mesmo nome a ser criada
            If Len(Dir(ActiveWorkbook.Path & "\" & Rng(r, c), vbDirectory)) = 0 Then
                
                ' Cria pasta com MkDir (Diret�rio da macro & "Prefiro a adicionar no nome da pasta" & Valor na c�lula)
                MkDir (ActiveWorkbook.Path & "\" & "" & Rng(r, c))
                On Error Resume Next
            End If
        r = r + 1
        Loop
    Next c
    
End Sub

