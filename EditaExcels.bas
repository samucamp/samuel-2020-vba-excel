Attribute VB_Name = "M�dulo1"
Sub Importar_XLS()
Attribute Importar_XLS.VB_ProcData.VB_Invoke_Func = "D\n14"

Dim sPath As String
Dim cell As Variant

'Para a macro executar mais r�pido!
With Application
    .ScreenUpdating = False
    .DisplayAlerts = False
End With

sPath = Localiza_Dir

'Arquivo excel para abrir
NomeArquivo = "Cadastro Geral"

' Onde a m�gica acontece
For Each cell In Worksheets("Planilha1").Range("A1", Range("A1").SpecialCells(xlCellTypeLastCell))
    Set Wkb = Workbooks.Open(sPath + "\" + Replace(cell, ",", ".") + "\" + NomeArquivo)
    InsereDeleta
    Wkb.Close Savechanges:=True
Next cell

' Retorna default
With Application
    .ScreenUpdating = True
    .DisplayAlerts = True
End With

End Sub

Private Function Localiza_Dir()
   Dim objShell, objFolder, chemin, SecuriteSlash
   Set objShell = CreateObject("Shell.Application")
   Set objFolder = _
   objShell.BrowseForFolder(&H0&, "Procurar por um Diret�rio", &H1&)
   On Error Resume Next
   chemin = objFolder.ParentFolder.ParseName(objFolder.Title).Path & ""
   If objFolder.Title = "Bureau" Then
      chemin = "C:WindowsBureau"
   End If
   If objFolder.Title = "" Then
      chemin = ""
   End If
   SecuriteSlash = InStr(objFolder.Title, ":")
   If SecuriteSlash > 0 Then
      chemin = Mid(objFolder.Title, SecuriteSlash - 1, 2) & ""
   End If
Localiza_Dir = chemin
End Function

Sub InsereDeleta()
    Columns("D").Insert
    Columns("E").Insert
    Columns("F").Insert
    Columns("C").Delete
    Range("C2").Select
    ActiveCell.FormulaR1C1 = "S/PRES"
    Range("D2").Select
    ActiveCell.FormulaR1C1 = "DANO"
    Range("E2").Select
    ActiveCell.FormulaR1C1 = "FALT"
    Range("E3").Select
End Sub
